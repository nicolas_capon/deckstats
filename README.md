# scryfall_api

Python interface for deckstats.net
Can generate deck url for deck list or load decklist from deckstats url

## How to import as a submodule

```bash
git submodule add https://gitlab.com/nicolas_capon/deckstats.git
```

To use in your python application import like this:
```python
from deckstats.deckstats import get_sealed_url
```

