from __future__ import annotations
from typing import List
import re
import json
import requests
import logging


class Card:
    name: str
    amount: int
    set_code: str

    def __init__(self, **kwargs) -> None:
        self.name = kwargs.get("name")
        self.amount = kwargs.get("amount", 1)
        self.set_code = kwargs.get("set_code")


class Deck:

    def __init__(
        self,
        cards: List[Card],
        title: str = "Deck",
        description: str = "",
        url: str = None,
    ) -> None:
        self.cards = cards
        self.title = title
        self.description = description
        self.url = url

    @classmethod
    def load_from_url(cls, url) -> Deck:
        """Load deck from target deckstat url"""
        p = re.compile(r"deckstats\.net/decks/")
        m = p.search(url)
        if not m:
            return False
        r = requests.get(url)
        if r.ok:
            data = r.content.decode("utf-8")
            p = re.compile(r"init_deck_data\((.*)\);deck_display")
            m = p.search(data)
            if not m:
                return False
            content = json.loads(m.group(1))
            cards = content.get("sections", [{}])[0].get("cards", [])
            title = content.get("name", None)
            p = re.compile(
                r'<div class="deck_text_editable_display_content deck_text_display_markdown">\n'
                "<p>(.*)<\/p>")
            m = p.search(data)
            description = None
            if m:
                description = m.group(1)
            return cls(cards=cards, title=title, description=description)

    def get_url(self) -> str:
        """Get deck url on deckstat.net from given deck.
        Code inspired from cockatrice:
        https://github.com/Cockatrice/Cockatrice/blob/master/cockatrice/src/deckstats_interface.cpp"""
        if self.url:
            return self.url
        if not self.cards:
            return None
        # Prepare request
        decklist = ""
        for i, deck_card in enumerate(self.cards):
            br = "\n"
            if hasattr(deck_card, "amount"):
                amount = deck_card.amount
            else:
                amount = 1
            if hasattr(deck_card, "note"):
                note = f" #{deck_card.note}"
            else:
                note = ""
            decklist += f"{amount}x {deck_card.name}{note}{br if i<len(self.cards)-1 else ''}"
        self.url = fetch_url(decklist, self.title)
        return self.url


def fetch_url(decklist, decktitle):
    url = "https://deckstats.net/index.php"
    headers = {"Content-type": "application/x-www-form-urlencoded"}
    data = {"deck": decklist, "decktitle": decktitle.encode("latin-1")}
    # Request and handle error status
    r = requests.post(url, data=data, headers=headers)
    if r.ok:
        # Regex to find deck url in page content
        regex = '<meta property="og:url" content="([^"]+)"'
        m = re.findall(regex, r.text)
        if m:
            return m[0]
        else:
            logging.info(
                f"Match not found in page content for deck [{decktitle}]")
            return None
    else:
        logging.info("Deckstat request failed {0}.".format(r))
        return None
